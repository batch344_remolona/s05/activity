import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();
        Contact contact1 = new Contact("Khyle", "(2)9629818", "makati city");
        Contact contact2 = new Contact();

        // Add both contacts to the phonebook
        phonebook.addContact(contact1);
        phonebook.addContact(contact2);

        if (phonebook.getContacts().isEmpty()) {
            System.out.println("\t Phonebook is empty.");
        } else {
            // Iterate through the contacts and display their details
            ArrayList<Contact> contacts = phonebook.getContacts();
            for (Contact contact : contacts) {
                System.out.println("\t CONTACT DETAILS");
                System.out.println("\t Name: " + contact.getName());
                System.out.println("\t Contact Number: " + contact.getContactNumber());
                System.out.println("\t Address: " + contact.getAddress());
                System.out.println();
            }
        }
    }
}
