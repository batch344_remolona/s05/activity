import java.util.ArrayList;

public class Phonebook {
    private ArrayList<Contact> contacts = new ArrayList<Contact>();

    // Default Constructor
    public Phonebook() {
        // Initialize any default values or setup here if needed
    }

    // Parameterized Constructor
    public Phonebook(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    // Getter method
    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    // Setter method
    public void addContact(Contact contact) {
        contacts.add(contact);
    }
}
